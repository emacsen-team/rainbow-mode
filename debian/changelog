rainbow-mode (1.0.6-2) unstable; urgency=medium

  * Team upload.
  * Rebuild against dh-elpa 2.1.5.
    Upload pushed to dgit-repos but not salsa.
    See <https://lists.debian.org/debian-emacsen/2024/07/msg00077.html>.

 -- Sean Whitton <spwhitton@spwhitton.name>  Fri, 26 Jul 2024 11:04:59 +0900

rainbow-mode (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * d/control: Declare Standards-Version 4.6.0 (no changes needed)
  * d/copyright: Bump copyright years
  * d/watch: Migrate to version 4
  * d/watch: Update regexp

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 05 May 2022 23:22:03 +0500

rainbow-mode (1.0.5-1) unstable; urgency=medium

  * New upstream version 1.0.5
  * d/control: Migrate to debhelper-compat 13
  * d/copyright: Update copyright years

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 01 Aug 2020 11:25:29 +0500

rainbow-mode (1.0.4-1) unstable; urgency=medium

  * New upstream version 1.0.4
  * d/control: Declare Standards-Verion 4.5.0 (no changes needed)

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 04 Apr 2020 16:57:33 +0500

rainbow-mode (1.0.3-1) unstable; urgency=medium

  * New upstream version 1.0.3

 -- Lev Lamberov <dogsleg@debian.org>  Sun, 29 Dec 2019 15:35:19 +0500

rainbow-mode (1.0.2-1) unstable; urgency=medium

  * New upstream version 1.0.2
  * Migrate to debhelper 12 without d/compat
  * d/control: Declare Standards-Version 4.4.1 (no changes needed)
  * d/control: Add Rules-Requires-Root: no
  * d/control: Drop emacs25 from Enhances
  * d/control: Drop Built-Using field
  * d/copyright: Bump copyright years
  * d/copyright: Add Upstream-Contact field

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 14 Dec 2019 22:33:15 +0500

rainbow-mode (1.0.1-2) unstable; urgency=medium

  * Team upload.
  * Rebuild with current dh-elpa

 -- David Bremner <bremner@debian.org>  Mon, 26 Aug 2019 13:48:34 -0300

rainbow-mode (1.0.1-1) unstable; urgency=medium

  * New upstream version 1.0.1
  * d/control: Update Maintainer field (to Debian Emacsen team)
  * d/copyright: Bump copyright years
  * d/copyright: Add Source field to d/copyright

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 26 May 2018 16:19:44 +0500

rainbow-mode (1.0-1) unstable; urgency=medium

  * New upstream version 1.0
  * Migrate to dh 11
  * d/control: Change Vcs-{Browser,Git} URL to salsa.debian.org
  * d/control: Declare Standards-Version 4.1.4 (no changes needed)
  * d/control: Enhance emacs25, not emacs24
  * d/control: Do not depend on emacs

 -- Lev Lamberov <dogsleg@debian.org>  Sat, 07 Apr 2018 14:18:45 +0500

rainbow-mode (0.13-1) unstable; urgency=medium

  * New upstream version 0.13
  * Bump copyright years
  * Don't pass --parallel to dh, compat is 10
  * Remove Testsuit declaration, there are no tests

 -- Lev Lamberov <dogsleg@debian.org>  Tue, 20 Jun 2017 17:25:54 +0500

rainbow-mode (0.12-1) unstable; urgency=medium

  * Initial release (Closes: #838584)

 -- Lev Lamberov <dogsleg@debian.org>  Thu, 22 Sep 2016 21:15:24 +0500
